#ifndef MAT_ELEM_H
#define MAT_ELEM_H
#include <iostream>

class MatElem {
private:
  double value;
  int row_num;
  int col_num;

public:
  MatElem(void);
  MatElem(double d, int p1, int p2);

  void SetMatElem(double d, int p1, int p2);

  double get_value() const;
  int get_row() const;
  int get_col() const;

  void show() const;
  MatElem transpose() const;

  //MatElem transpose();
  friend bool operator<(const MatElem& t1,const MatElem& t2);
  friend MatElem operator+(const MatElem& t1, const MatElem& t2);
  friend std::ostream &operator<<(std::ostream &output, const MatElem &m);
};
bool operator<(const MatElem& t1,const MatElem& t2);
MatElem operator+(const MatElem& t1, const MatElem& t2);
std::ostream &operator<<(std::ostream &output, const MatElem &m);

#endif //MAT_ELEM_H
