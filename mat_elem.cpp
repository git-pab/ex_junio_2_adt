#include "mat_elem.hpp"
#include <iostream>
#include <stdexcept>
MatElem::MatElem(void){}

MatElem::MatElem(double d, int p1, int p2) {
  SetMatElem(d, p1, p2);
}

void MatElem::SetMatElem(double d, int p1, int p2) {
  value = d;
  row_num = p1;
  col_num = p2;
}

double MatElem::get_value()const {
  return value;
}

int MatElem::get_row()const {
  return row_num;
}

int MatElem::get_col()const {
  return col_num;
}

void MatElem::show()const {
  std::cout << "(" << value << ", " << row_num << ", " << col_num << ")" ;
}

MatElem MatElem::transpose()const{
  return MatElem(value, col_num, row_num);
}

bool operator<(const MatElem &t1, const MatElem &t2) {
  return (t1.row_num < t2.row_num ||
	  (t1.row_num == t2.row_num && t1.col_num < t2.col_num));
}

MatElem operator+(const MatElem &t1, const MatElem &t2) {
  if (t1.row_num == t2.row_num && t1.col_num == t2.col_num)
    return MatElem(t1.value + t2.value, t1.row_num, t2.col_num);
  else
    throw std::invalid_argument("You can only add matrix elements that share position.");
}

std::ostream &operator<<(std::ostream &output, const MatElem &m) {
  output << "(" << m.value << ", " << m.row_num << ", " << m.col_num << ")";
  return output;
}
