#include <iostream>
#include <fstream>
#include "mat_elem.hpp"
#include "pabs_array.hpp"
#include "matrixkk.hpp"

int main () {
  pab::List<MatElem> a;
  MatElem m1(3.0, 0, 0);
  a.push_back(m1);
  MatElem m2(4.0, 1, 0);
  a.push_back(m2);
  MatElem m3(2.0, 1, 1);
  a.push_back(m3);
  MatrixKK<pab::List> ma(a, 1.0, 3, 3);
  std::cout << "ma = ";
  ma.show();
  std::cout << std::endl;

  pab::List<MatElem> b;
  MatElem m6(5.0, 0, 0);
  b.push_back(m6);
  MatElem m4(2.0, 0, 2);
  b.push_back(m4);
  MatElem m5(3.0, 2, 2);
  b.push_back(m5);
  MatrixKK<pab::List> mb(b, 1.0, 3, 3);
  std::cout << "mb = ";
  mb.show();
  std::cout << std::endl;

  std::ifstream file;
  file.open("mat.txt");
  MatrixKK<pab::List> ej1;
  file >> ej1;
  std::cout << "ej1 = ";
  ej1.show();
  std::cout << std::endl;
  MatrixKK<pab::List> ej1t;
  ej1t = ej1.transpose();
  std::cout << "ej1' = ";
  ej1t.show();
  std::cout << std::endl;
  file.close();
  
  MatrixKK<pab::List> mab = ma + mb;
  std::cout << "ma + mb = ";
  mab.show();
  std::cout << std::endl;

  MatrixKK<pab::List> mabt = mab.transpose();
  std::cout << "(ma + mb)' = ";
  mabt.show();
  std::cout << std::endl;

  std::ofstream outfile;
  outfile.open("mat_out.txt");
  outfile << mabt;
  outfile.close();
 
}
