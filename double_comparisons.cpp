#include "double_comparisons.hpp"
#include <cmath>

bool within_epsilon(double d1, double d2, double eps) 
{
  return fabs(d1-d2) < eps;
}
