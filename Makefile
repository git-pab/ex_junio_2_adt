CPP=g++
CPPFLAGS=-g -Wall

all: run_test_mat_elem

%.o: %.cpp %.hpp
	$(CPP) $(CPPFLAGS) -c $<

test_mat_elem: mat_elem.o double_comparisons.o test_mat_elem.cpp
	$(CPP) $(CPPFLAGS) -o $@ $^

test_array: mat_elem.o test_array.cpp
	$(CPP) $(CPPFLAGS) -o $@ $^

run_test_mat_elem: test_mat_elem
	./test_mat_elem

clean:
	rm *.o test_mat_elem test_array
