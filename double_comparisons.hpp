#ifndef DOUBLE_COMPARISONS_H
#define DOUBLE_COMPARISONS_H

#ifndef EPSILON
#define EPSILON 1.0/100.0
#include <cmath>

bool within_epsilon(double d1, double d2, double eps = EPSILON);

#endif //EPSILON
#endif //DOUBLE_COMPARISONS_H
