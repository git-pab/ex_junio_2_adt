#ifndef MATRIXKK_H
#define MATRIXKK_H

#include <iostream>
#include "mat_elem.hpp"
#include "double_comparisons.hpp"
#include "pabs_array.hpp"
#include "pabs_list.hpp"

//Forward declare classes and its friends

template <template <typename T> class Storage_Type> class MatrixKK;

template <template <typename T> class Storage_Type>
std::istream &operator>>(std::istream &in, MatrixKK<Storage_Type> &m);

template <template <typename T> class Storage_Type>
std::ostream &operator<<(std::ostream &out, const MatrixKK<Storage_Type> &m);

template <template <typename T> class Storage_Type>
MatrixKK<Storage_Type> operator+(const MatrixKK<Storage_Type> &m1,
				   const MatrixKK<Storage_Type> &m2);

//Declare the actual templates

template <template <typename T> class Storage_Type>
class MatrixKK
{
private:
  Storage_Type<MatElem> m;
  double v;
  int r;
  int c;

public:
  MatrixKK();
  MatrixKK(const Storage_Type<MatElem> &m, double v, int f, int c );
  void SetMatrixKK(const Storage_Type<MatElem> &m, double v, int f, int c );
  
  void show();
  MatrixKK transpose();

  friend MatrixKK operator+ <Storage_Type>(const MatrixKK<Storage_Type> &m1,
					     const MatrixKK<Storage_Type> &m2);
  friend std::istream &operator>> <Storage_Type> (std::istream &in,
						   MatrixKK<Storage_Type> &m);
  friend std::ostream &operator<< <Storage_Type> (std::ostream &out,
						   const MatrixKK<Storage_Type> &m);
  
};

template <template <typename T> class Storage_Type>
MatrixKK<Storage_Type> operator+(const MatrixKK<Storage_Type> &m1,
				 const MatrixKK<Storage_Type> &m2);

template <template <typename T> class Storage_Type>
std::istream &operator>>(std::istream &in, MatrixKK<Storage_Type> &m);

template <template <typename T> class Storage_Type>
std::ostream &operator<<(std::ostream &out, const MatrixKK<Storage_Type> &m);

//Implementation for class methods

template <template <typename T> class Storage_Type>
MatrixKK<Storage_Type>::MatrixKK(){};

template <template <typename T> class Storage_Type>
MatrixKK<Storage_Type>::MatrixKK(const Storage_Type<MatElem> &m,
				 double v, int f, int c )
{
  SetMatrixKK(m, v, f, c);
}

template <template <typename T> class Storage_Type>  
void MatrixKK<Storage_Type>::SetMatrixKK(const Storage_Type<MatElem> &m,
					 double v, int r, int c)
{
  this->m = m;
  this->v = v;
  this->r = r;
  this->c = c;
}

template <template <typename T> class Storage_Type>
void MatrixKK<Storage_Type>::show()
{
  std::cout << "(" << m << ", " << v << ", " << r << ", " << c << ")";
}

template <template <typename T> class Storage_Type>
MatrixKK<Storage_Type> MatrixKK<Storage_Type>::transpose()
{
  Storage_Type<MatElem> t;
  for (int i = 0; i < m.len(); i++)
    t.push_back(m[i].transpose());
  return MatrixKK(t.mergesort(), v, r, c);
}

//Friend Implementations

template <template <typename T> class Storage_Type>
MatrixKK<Storage_Type> operator+(const MatrixKK<Storage_Type> &m1,
				 const MatrixKK<Storage_Type> &m2)
{
  if (m1.r == m2.r && m1.c == m2.c) {
    MatrixKK<Storage_Type> m;
    m.v = m1.v + m2.v;
    m.r = m1.r;
    m.c = m1.c;
    int i1 = 0, i2 = 0;
    MatElem e;
    while (i1 < m1.m.len() && i2 < m2.m.len()) {
      if (m1.m[i1] < m2.m[i2]) {
	e.SetMatElem(m1.m[i1].get_value() + m2.v,
		     m1.m[i1].get_row(),
		     m1.m[i1].get_col());
	m.m.push_back(e);
	i1++;
      } else if (m2.m[i2] < m1.m[i1]) {
	e.SetMatElem(m2.m[i2].get_value() + m1.v,
		     m2.m[i2].get_row(),
		     m2.m[i2].get_col());
	m.m.push_back(e);
	i2++;
      } else {
	e.SetMatElem(m1.m[i1].get_value() + m2.m[i2].get_value(),
		     m1.m[i1].get_row(),
		     m1.m[i1].get_col());
	if (!within_epsilon(e.get_value(), m.v))
	  m.m.push_back(e);
	i1++;
	i2++;
      }
    }
    while (i1 < m1.m.len()) {
      e.SetMatElem(m1.m[i1].get_value() + m2.v,
		   m1.m[i1].get_row(),
		   m1.m[i1].get_col());
      m.m.push_back(e);
      i1++;
    }
    while (i2 < m2.m.len()) {
      e.SetMatElem(m2.m[i2].get_value() + m1.v,
		   m2.m[i2].get_row(),
		   m2.m[i2].get_col());
      m.m.push_back(e);
      i2++;
    }
    return m;
  } else {
    throw std::invalid_argument("Matrices with diferent sizes can't be added.");
  }
}

template <>
MatrixKK<pab::List> operator+(const MatrixKK<pab::List> &m1,
			       const MatrixKK<pab::List> &m2)
{
  if (m1.r == m2.r && m1.c == m2.c) {
    MatrixKK<pab::List> m;
    m.v = m1.v + m2.v;
    m.r = m1.r;
    m.c = m1.c;
    int i1 = 0, i2 = 0;
    MatElem e;
    while (i1 < m1.m.len() && i2 < m2.m.len()) {
      if (m1.m[i1] < m2.m[i2]) {
	e.SetMatElem(m1.m[i1].get_value() + m2.v,
		     m1.m[i1].get_row(),
		     m1.m[i1].get_col());
	m.m.push_front(e);
	i1++;
      } else if (m2.m[i2] < m1.m[i1]) {
	e.SetMatElem(m2.m[i2].get_value() + m1.v,
		     m2.m[i2].get_row(),
		     m2.m[i2].get_col());
	m.m.push_front(e);
	i2++;
      } else {
	e.SetMatElem(m1.m[i1].get_value() + m2.m[i2].get_value(),
		     m1.m[i1].get_row(),
		     m1.m[i1].get_col());
	if (!within_epsilon(e.get_value(), m.v))
	  m.m.push_front(e);
	i1++;
	i2++;
      }
    }
    while (i1 < m1.m.len()) {
      e.SetMatElem(m1.m[i1].get_value() + m2.v,
		   m1.m[i1].get_row(),
		   m1.m[i1].get_col());
      m.m.push_front(e);
      i1++;
    }
    while (i2 < m2.m.len()) {
      e.SetMatElem(m2.m[i2].get_value() + m1.v,
		   m2.m[i2].get_row(),
		   m2.m[i2].get_col());
      m.m.push_front(e);
      i2++;
    }
    m.m = m.m.reverse();
    return m;
  } else {
    throw std::invalid_argument("Matrices with diferent sizes can't be added.");
  }
}

template <>
MatrixKK<pab::Array> operator+(const MatrixKK<pab::Array> &m1,
			       const MatrixKK<pab::Array> &m2)
{
  if (m1.r == m2.r && m1.c == m2.c) {
    MatrixKK<pab::Array> m;
    m.v = m1.v + m2.v;
    m.r = m1.r;
    m.c = m1.c;
    int i1 = 0, i2 = 0;
    MatElem e;
    while (i1 < m1.m.len() && i2 < m2.m.len()) {
      if (m1.m[i1] < m2.m[i2]) {
	e.SetMatElem(m1.m[i1].get_value() + m2.v,
		     m1.m[i1].get_row(),
		     m1.m[i1].get_col());
	m.m.push_back(e);
	i1++;
      } else if (m2.m[i2] < m1.m[i1]) {
	e.SetMatElem(m2.m[i2].get_value() + m1.v,
		     m2.m[i2].get_row(),
		     m2.m[i2].get_col());
	m.m.push_back(e);
	i2++;
      } else {
	e.SetMatElem(m1.m[i1].get_value() + m2.m[i2].get_value(),
		     m1.m[i1].get_row(),
		     m1.m[i1].get_col());
	if (!within_epsilon(e.get_value(), m.v))
	  m.m.push_back(e);
	i1++;
	i2++;
      }
    }
    while (i1 < m1.m.len()) {
      e.SetMatElem(m1.m[i1].get_value() + m2.v,
		   m1.m[i1].get_row(),
		   m1.m[i1].get_col());
      m.m.push_back(e);
      i1++;
    }
    while (i2 < m2.m.len()) {
      e.SetMatElem(m2.m[i2].get_value() + m1.v,
		   m2.m[i2].get_row(),
		   m2.m[i2].get_col());
      m.m.push_back(e);
      i2++;
    }
    return m;
  } else {
    throw std::invalid_argument("Matrices with diferent sizes can't be added.");
  }
}

template <template <typename T> class Storage_Type>
std::istream &operator>>(std::istream &in, MatrixKK<Storage_Type> &m)
{
  in >> m.v >> m.r >> m.c;
  double d;
  MatElem new_elem;
  for (int i = 0; i < m.r; i++) {
    for (int j = 0; j < m.c; j++) {
      in >> d;
      if (d != m.v){
        new_elem.SetMatElem(d, i, j);
	m.m.push_back(new_elem);
      }
    }
  }
  return in;
}

template <template <typename T> class Storage_Type>
std::ostream &operator<<(std::ostream &out, const MatrixKK<Storage_Type> &m)
{
  out << m.v << "\n" << m.r << "\n" << m.c << "\n";
  int i = 0, j = 0;
  int i_p, j_p;
  for (int e = 0; e < m.m.len(); e++) {
    i_p = m.m[e].get_row();
    j_p = m.m[e].get_col();
    while (i < i_p) {
      while (j < m.c) {
	out << m.v << " ";
	j++;
      }
      out << "\n";
      j = 0;
      i++;
    }
    while (j < j_p) {
      out << m.v << " ";
      j++;
    }
    out << m.m[e].get_value() << " ";
    if (j == m.c) {
      out << "\n";
      j = 0;
      i++;
    } else {
      j++;
    }
  }
  while (i < m.r) {
    while (j < m.c) {
      out << m.v << " ";
      j++;
    }
    out << "\n";
    j = 0;
    i++;
  }
  return out;
}

#endif //MATRIXKK_H
