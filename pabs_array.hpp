#ifndef PABS_ARRAY_H
#define PABS_ARRAY_H

#include <iostream>

namespace pab 
{
  template <typename T>
  class Array
  {
  private:
    T *entries;
    int num_entries;
    int max_capacity;
    
  public:
    Array();
    ~Array();
    Array(const Array<T> &m);
    Array<T> &operator= (const Array<T> &m);
  
    Array<T>& push_front(T &m);
    Array<T>& push_back(const T &m);
    Array<T>& insert(int i, const T &e);
    Array<T> slice(int first, int last) const;
    Array<T> mergesort();
    Array<T> merge(const Array<T> &m1);
    T& operator[](int i) const;
    int len(void) const;

    friend std::ostream &operator<<(std::ostream &output,const Array<T> &m) {
      output << "[" << m[0];
      for (int i = 1; i < m.num_entries; i++) {
	output << ", " << m[i];
      }
      output << "]";
      return output;
    }
  };
  template <typename T>
  std::ostream &operator<<(std::ostream &output,const Array<T> &m);
}
template <typename T>
pab::Array<T>::Array() {
  max_capacity = 2;
  entries = new T[max_capacity];
  num_entries = 0;
}

template <typename T>
pab::Array<T>::~Array() {
  delete [] entries;
}

template <typename T>
pab::Array<T>::Array(const pab::Array<T> &m) {
  max_capacity = m.max_capacity;
  entries = new T[max_capacity];
  num_entries = m.num_entries;
  for (int i = 0; i < m.num_entries; i++) {
    entries[i] = m.entries[i];
  }
}

template <typename T>
pab::Array<T> &pab::Array<T>::operator= (const pab::Array<T> &m) {
  max_capacity = m.max_capacity;
  delete [] entries;
  entries = new T[max_capacity];
  num_entries = m.num_entries;
  for (int i = 0; i < m.num_entries; i++) {
    entries[i] = m.entries[i];
  }
  return *this;
}

template <typename T>
pab::Array<T> &pab::Array<T>::push_back(const T &m) {
  T *tmp;
  if (max_capacity == num_entries) {
    tmp = new T[max_capacity*2];
    for (int i = 0; i < num_entries; i++)
      tmp[i] = entries[i];
    max_capacity*=2;
    delete [] entries;
    entries = tmp;
  }
  entries[num_entries] = m;
  num_entries++;
  return *this;
}

template <typename T>
pab::Array<T> pab::Array<T>::slice(int first, int last)const {
  pab::Array<T> sliced;
  if (last < first)
    throw std::out_of_range("Invalid slice range.");
  else if(first < 0)
    throw std::out_of_range("Negative slice range.");
  else if(num_entries < last)
    throw std::out_of_range("Index out of bounds in slice.");
  for (int i = first; i < last; i++)
    sliced.push_back(entries[i]);
  return sliced;
}

template <typename T>
T& pab::Array<T>::operator[](int i) const {
  if (num_entries > i)
    return entries[i];
  else
    throw std::out_of_range("Array number of entries exceeded.");
}

template <typename T>
int pab::Array<T>::len(void)const{
  return num_entries;
}

template <typename T>
pab::Array<T> pab::Array<T>::mergesort() {
  if (len() == 1){
    pab::Array<T> sorted;
    sorted.push_back((*this)[0]);
    return sorted;
  }
  else {
    pab::Array<T> p1 = slice(0, len() / 2);
    pab::Array<T> p2 = slice(len() / 2, len());
    pab::Array<T> psort1 = p1.mergesort();
    pab::Array<T> psort2 = p2.mergesort();
    pab::Array<T> ps12 = psort1.merge(psort2);
    return ps12;
  }
}
template <typename T>
pab::Array<T> pab::Array<T>::merge(const pab::Array<T> &m2) {
  pab::Array<T> m;
  int i1 = 0, i2 = 0;
  while (i1 < this->len() && i2 < m2.len()) {
    if ((*this)[i1] < m2[i2]) {
      m.push_back((*this)[i1]);
      i1++;
    } else if (m2[i2] < (*this)[i1]) {
      m.push_back(m2[i2]);
      i2++;
    } else {
      m.push_back((*this)[i1]);
      i1++;
      m.push_back(m2[i2]);
      i2++;
    }
  }
  while (i1 < this->len()) {
    m.push_back((*this)[i1]);
    i1++;
  }
  while (i2 < m2.len()) {
    m.push_back(m2[i2]);
    i2++;
  }
  return m;
}
#endif //PABS_ARRAY_H
